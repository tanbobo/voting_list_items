# 投票排行榜项目

#### 项目介绍
投票排行榜
![输入图片说明](https://images.gitee.com/uploads/images/2018/1121/181529_992147d5_1547319.png "屏幕截图.png")
#### 软件架构
软件架构说明

SpringBoot2.0.3+Mysql+redis+mybatis

#### 安装教程

1. 数据库表结构

CREATE TABLE `t_dtb_producer_rank` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `code` varchar(30) NOT NULL,
  `vote_num` int(20) NOT NULL,
  `rank` int(20) NOT NULL,
  `faken_num` int(20) NOT NULL,
  `faken_money` int(11) NOT NULL,
  `influence_power` int(10) NOT NULL DEFAULT '0' COMMENT '基础票',
  `type` int(5) NOT NULL COMMENT '投票类型',
  `status` int(6) NOT NULL COMMENT '是否参加投票',
  `is_vote` int(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8

2. 调用 /vote/create 接口，创建几个对象，进行排行

3. 调用 /vote/vote 接口 进行投票

4. 调用 /vote/flushCacheToMysql 将最新排名的redis缓存到mysql中

5 多个接口，如果有疑问，请在开源中国私信我：木九天



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)shu