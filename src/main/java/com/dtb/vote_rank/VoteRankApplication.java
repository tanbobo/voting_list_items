package com.dtb.vote_rank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VoteRankApplication {

    public static void main(String[] args) {
        SpringApplication.run(VoteRankApplication.class, args);
    }
}
