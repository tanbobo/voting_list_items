package com.dtb.vote_rank.cache;

import redis.clients.jedis.Tuple;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @Description：工具类
 * @Date：2018/11/15 下午2:52
 * @Author：ChengJian
 * @UpdateRemark:
 * @Version:1.0
 *
 */

public interface ICacheService {

	/**
	 * 设置键值
	 * @param key
	 * @param value
	 * @return
	 */
	String set(String key, String value);

	/**
	 * 
	 * @param key
	 * @param increament
	 * @return
	 */
	Long incrBy(String key, long increament);

	/**
	 * 
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	Long hset(String key, String field, String value);

	/**
	 * 
	 * @param key
	 * @return
	 */
	String get(String key);

	/**
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	String hget(String key, String field) ;
	
	/**
	 * 
	 * setex(写入缓存)
	 * @param key 
	 * @param seconds 过期时间 单位秒
	 * @param value
	 * void
	 * @exception
	 * @since  1.0.0
	 */
	String setex(String key, int seconds, String value);

	/**
	 * @param key
	 * @return Set<String>
	 * @exception
	 * @since  1.0.0
	 */
	Set<String> keys(String key);

	/**
	 *
	 * get(返回指定key值)
	 * @param key
	 * @return
	 * String
	 * @exception
	 * @since  1.0.0
	 */
	List<String> mget(String... params);

	/**
	 * 判断是否有某个key
	 * @param key
	 * @return
	 * String
	 * @exception
	 * @since  1.0.0
	 */
	boolean exists(String key);

	/**
	 *
	 * get(返回指定key值)
	 * @param key
	 * @return
	 * String
	 * @exception
	 * @since  1.0.0
	 */
	void del(String key);

	long dels(String... keys);

	/**
	 *
	 * push(在指定链表头部增加一个或多个值，链表不存在则新建)
	 * @param queueName 队列名
	 * @param values 值顺序列表，如：a,b,c.先进先出
	 * @return Long 队列元素个数
	 * @exception
	 * @since  1.0.0
	 */
	Long push(String queueName, String... values);

	/**
	 *
	 * pop(从指定队列表尾部取出一个值)
	 * @param queueName 队列名
	 * @return String 元素值
	 * @exception
	 * @since  1.0.0
	 */
	String pop(String queueName);

	/**
	 *
	 * lrange(从指定队列取起始位置到结束位置元素)
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 * List<String>
	 * @exception
	 * @since  1.0.0
	 */
	List<String> lrange(String key, long start, long end);

	/**
	 *
	 * lrange(从指定队列取起始位置到结束位置元素)
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 * List<String>
	 * @exception
	 * @since  1.0.0
	 */
	long llen(String key);

	void hmset(String key, Map<String, String> value);

	Map<String,String> hget(String key);

	List<String> hmget(String key, String... fields);

	Long hdel(String key, String... fields);

	/**
	 * true 存在 false 不存在
	 * @param key
	 * @param field
	 * @return
	 */
	boolean hexists(String key, String field);

	boolean hincrBy(String key, String field, Long value);

	/**
	 *
	 * queueLength(返回指定队列元素个数)
	 * @param queueName
	 * @return Long
	 * @exception
	 * @since  1.0.0
	 */
	Long queueLength(String queueName);


	/**
	 *
	 * zadd(向有序sorts-set里添加值)
	 * @param key 指定key
	 * @param score 分数，排序用
	 * @param member 成员(要存的值)
	 * void
	 * @exception
	 * @since  1.0.0
	 */
	Long zadd(String key, double score, String member);

	/**
	 * sadd	集合(Set)	(向集合添加一个或多个成员)
	 * @param key
	 * @param member
	 */
	Long sadd(String key, String member);

	Long smove(String srckey, String dstkey, String member);

	Long scard(String key);

	Long srem(String key, String member);

	Long zrem(String key, String member);

	/**
	 * SET 集合
	 * 判断成员元素是否是集合的成员
	 * @param key
	 * @param member
	 * @return
	 */
	Boolean sismember(String key, String member);

	/**
	 * SET 集合
	 * 获取集合的成员
	 * @param key
	 * @param member
	 * @return
	 */
	Set<String> smembers(String key);

	/**
	 * SET 集合
	 * 获取集合的成员
	 * @param key
	 * @param member
	 * @return
	 */
	Set<String> sinter(String... keys);

	/**
	 *
	 * zincress(向有序sorts-set里添加值)
	 * @param key 指定key
	 * @param score 分数，排序用
	 * @param member 成员(要存的值)
	 * void
	 * @exception
	 * @since  1.0.0
	 */
	Double zincress(String key, double score, String member);

	/**
	 *
	 * zcard(返回key Sets数量)
	 * @param key
	 * @return
	 * Long
	 * @exception
	 * @since  1.0.0
	 */
	Long zcard(String key);

	/**
	 *
	 * zrange(返回有序list，以score正序排，1，2，3，4等 )
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 * List<String>
	 * @exception
	 * @since  1.0.0
	 */
	Set<String> zrange(String key, final long start, final long end);

	List<Tuple> zrangeWithScores(String key, final long start, final long end);

	Long zrevrank(String key, String member) ;

	/**
	 *
	 * zrevrangeByScore(返回有续集key中score<=max并且score>=min 的元素，返回结果根据score从大到小顺序排列 )
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 * List<String>
	 * @exception
	 * @since  1.0.0
	 */
	List<String> zrevrangeByScore(String key, final double min, final double max, int skip, int size);

	/**
	 *  zrevrange 命令返回有序集中，指定区间内的成员。
	 *  其中成员的位置按分数值递减(从大到小)来排列。
	 *  具有相同分数值的成员按字典序的逆序(reverse lexicographical order)排列。
	 *  除了成员按分数值递减的次序排列这一点外，
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Set<Tuple> zrevrangeWithScorese(String key, final long start, final long end);


	void zrem(String key, String... members);

	/**
	 *
	 * zscore(返回某个成员的分数，如果不存在返回null)
	 * @param key
	 * @param member
	 * @return
	 * Double
	 * @exception
	 * @since  1.0.0
	 */
	Double zscore(String key, String member);

	/**
	 * 有序集合中指定成员的分数加上增量
	 * @param key
	 * @param score
	 * @param member
	 * @return
	 */
	Double zincrby(String key, double score, String member);

	/**
	 * incr(按key自增(+1))
	 * @param key
	 * @return
	 * Long
	 * @exception
	 * @since  1.0.0
	 */
	Long incr(String key);

	/**
	 * redis锁
	 * @param key
	 * @param value
	 * @return
	 */
	Long setnx(String key, String value);

	/**
	 * 存放List
	 * @param key
	 * @param value
	 * @return
	 */
	Long rpush(String key, String value) ;

	/**
	 * 获取List
	 * @param key
	 * @param start
	 * @param count -1为全部
	 * @return
	 */
	List<String> sort(String key, int start, int count);

	/**
	 * 发布
	 * @param channel
	 * @param message
	 * @return
	 */
	Long publish(String channel, String message);

	/**
	 * 获取值过期时间
	 * @param key
	 * @return
	 */
	Long ttl(String key);

	List<String >srandmember(String key, int count);

	long hlen(String key);
}
