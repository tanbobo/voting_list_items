package com.dtb.vote_rank.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @Author:ChengJian
 * @Description:
 * @Date: Created in 下午3:12 2018/11/15
 */
@Data
@ToString
public class ProducerEntity {

    private int id;
    //名称
    private String name;
    //编号
    private String code;
    //投票数量
    private long voteNum;
    //排行榜
    private long rank;
    //造假票数
    private int fakenNum;
    //造假金额
    private double fakenMoney;
    //其他影响力，基础分
    private int influencePower;
    //排行榜类型
    private int type;
    //是否参加,是否通过审核 1 通过 2未通过
    private int status;
    //今天是否参加投票
    private int isVote;
}
