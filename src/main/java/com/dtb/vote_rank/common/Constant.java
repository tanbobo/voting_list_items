package com.dtb.vote_rank.common;

/**
 * @Author:ChengJian
 * @Description:
 * @Date: Created in 下午2:28 2018/11/21
 */
public class Constant {

    //投票活动是否开启，1开启，2关闭
    public static final String ACTIVITY_IS_VOTE_KEY = "activity_is_vote_key";

    //用户投票专属id,保障用户只能投票一次
    public static final String VOTE_FLAG = "user_vote_flag";
}
