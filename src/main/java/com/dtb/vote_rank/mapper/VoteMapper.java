package com.dtb.vote_rank.mapper;

import com.dtb.vote_rank.entity.ProducerEntity;

import java.util.List;
import java.util.Map;

/**
 * @Author:ChengJian
 * @Description:
 * @Date: Created in 上午10:10 2018/11/16
 */
public interface VoteMapper {
    List<ProducerEntity> getList(Map<String,Object> map);
}
