package com.dtb.vote_rank.dao;

import com.dtb.vote_rank.entity.ProducerEntity;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

/**
 * @Author:ChengJian
 * @Description:
 * @Date: Created in 下午3:57 2018/11/15
 */
@Mapper
public interface VoteDao {

    @Insert({
            "insert into t_dtb_producer_rank(name,code,vote_num,rank,faken_num,faken_money,influence_power,type,status)" +
                    " values(#{name},#{code},#{voteNum},#{rank},#{fakenNum},#{fakenMoney},#{influencePower},#{type},#{status})"
    })
    void addProducer(ProducerEntity producerEntity);

    @Select({
            "select id,name,code,vote_num,rank,faken_num,faken_money,influence_power,type,status from t_dtb_producer_rank where" +
                    " id = #{id}"
    })
    @Results({
            @Result(column = "id",property ="id",jdbcType = JdbcType.INTEGER,id = true),
            @Result(column = "name",property = "name",jdbcType = JdbcType.VARCHAR),
            @Result(column = "code",property = "code",jdbcType = JdbcType.VARCHAR),
            @Result(column = "vote_num",property = "voteNum",jdbcType = JdbcType.INTEGER),
            @Result(column = "rank",property = "rank",jdbcType = JdbcType.INTEGER),
            @Result(column = "faken_num",property = "fakenNum",jdbcType = JdbcType.INTEGER),
            @Result(column = "faken_money",property = "fakenMoney",jdbcType = JdbcType.DOUBLE),
            @Result(column = "influcence_power",property = "influencePower",jdbcType = JdbcType.INTEGER),
            @Result(column = "type",property = "type",jdbcType = JdbcType.INTEGER),
            @Result(column = "status",property = "status",jdbcType = JdbcType.INTEGER),
    })
    ProducerEntity getProducer(@Param("id")int id);

    @Update({
            "update t_dtb_producer_rank set vote_num =#{voteNum},rank=#{rank} where id =#{id}"
    })
    int updateProduer(ProducerEntity producerEntity);

    List<ProducerEntity> getList(Map<String,Object> map);

    @Delete({
            "delete from t_dtb_producer_rank where id =#{id}"
    })
    int delete(@Param("id")int id);

    @Select({
            "select * from t_dtb_producer_rank where name =#{arg0} and code =#{arg1}"
    })
    ProducerEntity getNewProducer(String name,String code);
}
